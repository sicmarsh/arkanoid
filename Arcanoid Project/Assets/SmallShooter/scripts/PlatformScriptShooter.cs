﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlatformScriptShooter : MonoBehaviour
{
    public GameObject[] Balls; //R-red, G-green, B-blue, Y - yellow, D - Gold
    public GameObject[] SignalBalls;//шары без коллизии
    public Transform ShootPoint;
    public GameObject winGates;
    private float[] BulletsMagazine = new float[] { 20, 20, 20, 20, 20 };
    public RawImage[] MagazineUI;
    [SerializeField] private float ShootSpeed;
    [SerializeField] private float ShootForce;
    [SerializeField] private float MoveSpeed;

    GameObject currentSignalBall;

    int curentBallType;
    int prevBallType;

    float nextShotTimer;

    void Start()
    {
        curentBallType = 1;
        foreach (var ammo in MagazineUI)
        {
            ammo.transform.localScale = new Vector3(0.3f, BulletsMagazine[curentBallType] / 10, 1);
        }

    }

    void FixedUpdate()
    {
        //двигаем обьект вперед
        transform.Translate(Vector3.forward * MoveSpeed, Space.World);

        //движение влево/вправо
        float horizontalInput = Input.GetAxis("Mouse X");
        if (horizontalInput < 0 && transform.position.x > -3.8f)
        {
            transform.Translate(0.4f * horizontalInput, 0, 0, Space.World);
        }

        if (horizontalInput > 0 && transform.position.x < 3.8f)
        {
            transform.Translate(0.4f * horizontalInput, 0, 0, Space.World);
        }

        //стрельба
        if (Input.GetButton("Fire1") && Time.time > nextShotTimer)
        {
            if (BulletsMagazine[curentBallType] == 0)
            {
                return;
            }
            GameObject obj = Instantiate(Balls[curentBallType], ShootPoint.position, ShootPoint.rotation) as GameObject;
            obj.GetComponent<Rigidbody>().AddForce(Vector3.forward * ShootForce, ForceMode.Impulse);
            Destroy(obj, 2f);
            nextShotTimer = Time.time + ShootSpeed;
            BulletsMagazine[curentBallType]--;
            MagazineUI[curentBallType].transform.localScale = new Vector3(0.3f, BulletsMagazine[curentBallType] / 10, 1);
        }

        //смена типа шара
        if (Input.GetKeyDown(KeyCode.R)) curentBallType = 0;
        if (Input.GetKeyDown(KeyCode.G)) curentBallType = 1;
        if (Input.GetKeyDown(KeyCode.B)) curentBallType = 2;
        if (Input.GetKeyDown(KeyCode.Y)) curentBallType = 3;
        if (Input.GetKeyDown(KeyCode.D)) curentBallType = 4;

        //смена сигнального шара
        if (curentBallType != prevBallType || currentSignalBall == null)
        {
            Destroy(currentSignalBall);
            currentSignalBall = Instantiate(SignalBalls[curentBallType], ShootPoint.position, ShootPoint.rotation) as GameObject;
            currentSignalBall.transform.parent = transform;
        }
        prevBallType = curentBallType;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ammo"))
        {
            for( int c = 0; c < BulletsMagazine.Length; c++)
            {
                BulletsMagazine[c] = 20;
            }
        }
    }
}



