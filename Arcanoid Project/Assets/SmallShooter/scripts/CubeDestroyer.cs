﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeDestroyer : MonoBehaviour
    
{
    [SerializeField] private float DestroyTime;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name != "Plane")
        {
            
            Destroy(gameObject, DestroyTime );
        }
    
}
}
