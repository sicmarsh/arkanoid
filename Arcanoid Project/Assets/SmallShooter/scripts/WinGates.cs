﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinGates : MonoBehaviour
{
    public GameObject text_prefab;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Win!!!");
            Instantiate(text_prefab);
        }
    }
}
