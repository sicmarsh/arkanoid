﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class SpawnEffect : MonoBehaviour
{
    [SerializeField] private GameObject m_fxPrefab;
    [SerializeField] private GameObject m_ammopack;
    System.Random random = new System.Random();

    public void SpawnFX()
    {
        Vector3 pos = transform.position;
        Quaternion rot = transform.rotation;
        Instantiate(m_fxPrefab, pos, rot);
        if (Convert.ToBoolean(random.Next(-1, 2)))
        {
            Instantiate(m_ammopack, pos, rot);
        }
        
    }
}
