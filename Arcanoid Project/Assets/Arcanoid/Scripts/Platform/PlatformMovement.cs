﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement : MonoBehaviour
{
    [SerializeField] private float m_BasicSpeed = 10f;
    private float m_moveSpeed;
    [SerializeField] private float m_rightBound;
    [SerializeField] private float m_leftBound;

    private void Awake()
    {
        m_moveSpeed = m_BasicSpeed;
}
    private void Update()
    {        
        Vector3 translation = Input.GetAxis("Horizontal") * Vector3.right * Time.deltaTime * m_moveSpeed;
        Vector3 newPos = transform.position + translation;

        newPos.x = Mathf.Clamp(newPos.x, m_leftBound, m_rightBound);

        transform.position = newPos;
        
    }

    public void BonusSpeed(bool engage, float SpeedBonus)
    {
        if (engage)
        {
            m_moveSpeed = m_moveSpeed * SpeedBonus;
        }
        else
        {
            m_moveSpeed = m_BasicSpeed;
        }        
    }
}
