﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformShooter : MonoBehaviour
{

    [SerializeField] private float m_pushForce = 5f;
    [SerializeField] private Rigidbody m_ballPrefab;
    [SerializeField] private Transform m_spawnPlace;

    private Rigidbody m_rbBall;

    private void Start()
    {
        TrySpawnBall();
    }

    private void TrySpawnBall()
    {
        if(m_ballPrefab != null)
        {
            m_rbBall = Instantiate(m_ballPrefab, m_spawnPlace.position, m_spawnPlace.rotation, m_spawnPlace);
            m_rbBall.isKinematic = true;
        }
        else
        {
            Debug.Log("Ball prefab is Empty");
        }
    }

    private void FixedUpdate()
    {
        if (GameController.StopGame)
        {
            if (!m_rbBall.isKinematic)
            {
                m_rbBall.isKinematic = true;
                m_rbBall.transform.position = m_spawnPlace.position;
                m_rbBall.transform.parent = m_spawnPlace;
            }

            if (Input.GetKeyDown(KeyCode.Space) && m_ballPrefab.isKinematic)
            {
                TryPushBall();
            }
        }
    }

    private void TryPushBall()
    {
        if (m_rbBall != null)
        {
            GameController.StopGame = false;
            m_rbBall.isKinematic = false;
            m_rbBall.transform.parent = null;
            m_rbBall.AddForce(m_rbBall.transform.forward * m_pushForce, ForceMode.Impulse);
        }
    }
}
