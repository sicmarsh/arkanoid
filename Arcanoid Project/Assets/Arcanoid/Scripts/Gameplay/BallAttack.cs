﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallAttack : MonoBehaviour
{
    [SerializeField] private int m_damage;

    private void OnCollisionEnter(Collision collision)
    {
        TrySetDamage(collision.gameObject);
    }

    private void TrySetDamage(GameObject gameObject)
    {
        ObjectHealth health = gameObject.GetComponent<ObjectHealth>();
        if(health != null)
        {
            health.Health -= m_damage;
        }
    }    
}
