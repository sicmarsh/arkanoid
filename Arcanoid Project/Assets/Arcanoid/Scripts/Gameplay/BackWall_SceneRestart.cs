﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackWall_SceneRestart : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            if (GameObject.FindGameObjectsWithTag("Ball").Length > 1)
            {
                Destroy(other);
            }
            else
            {
                GameController.PlayerHealth--;
                GameController.StopGame = true;
            }
        }
    }
}
