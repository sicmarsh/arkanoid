﻿using UnityEngine;

public class ShieldSwitcher : MonoBehaviour
{
    [SerializeField] private GameObject m_shield;
    [SerializeField] private GameObject m_bigShield;

    public void SwitchShield (bool isBig)
    {
        m_shield.gameObject.SetActive(!isBig);
        m_bigShield.gameObject.SetActive(isBig);
    }
}
