﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static bool StopGame;
    public static bool IsVictory;
    public static bool IsDefeat;
    public static int PlayerHealth;

    private float m_timer = 1f;

    private void Start()
    {
        ResetGameState();
    }

    private void Update()
    {
        if(Time.time < m_timer)
        {
            return;
        }

        m_timer = Time.time + 1;

        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        var isEndOfGame = enemies.Length == 0 || PlayerHealth <= 0;

        if (isEndOfGame)
        {
            if (PlayerHealth <= 0) IsVictory = false;
            else if (enemies.Length == 0) IsVictory = true;
   
            StopBalls();

            if (IsVictory)
            {
                Debug.Log("You win!");
            }
            else
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }

    private static void StopBalls()
    {
        var balls = GameObject.FindGameObjectsWithTag("Ball");

        foreach (var ball in balls)
        {
            ball.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    private void ResetGameState()
    {
        StopGame = true;
        IsVictory = false;
        IsDefeat = false;
        PlayerHealth = 3;
    }
}
