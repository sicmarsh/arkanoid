﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(Collider), typeof(MeshFilter))]
public class ObjectHealth : MonoBehaviour
{
    [SerializeField] private int m_health = 1;
    [SerializeField] private Mesh[] m_meshes;
    [SerializeField] private GameObject[] m_replacingObjects;

    [Header("Autodestroy")]
    [SerializeField] private bool m_isDestroyAfterReplacing;
    [SerializeField] private float m_destroyTime;

    private Rigidbody m_rb;
    private Collider m_collider;
    private MeshFilter m_meshFilter;
    private int m_prevHealth;

    public int Health
    {
        get { return m_health; }
        set { m_health = value; }
    }

    private void Start()
    {
        m_prevHealth = Health;
        m_rb = GetComponent<Rigidbody>();
        m_collider = GetComponent<Collider>();
        m_meshFilter = GetComponent<MeshFilter>();
    }

    private void Update()
    {
        if (transform.position.y > 1f)
        {
            m_rb.WakeUp();
        }
        if (m_prevHealth == Health)
        {
            return;
        }
        TryChangeMesh();
        TryDestroyObjectAndReplace();
    }

    private void TryChangeMesh()
    {
        if (m_meshes.Length > 0)
        {
            int index = m_meshes.Length - m_health;
            if (index >= 0 && index < m_meshes.Length)
            {
                m_meshFilter.mesh = m_meshes[index];
            }
        }
    }
    

    private void TryDestroyObjectAndReplace()
    {
        if (m_health <= 0 && m_collider.enabled)
        {
            m_collider.enabled = false;
            m_rb.useGravity = false;

            if (m_replacingObjects != null)
            {
                foreach(GameObject prefab in m_replacingObjects)
                {
                    GameObject newObj = Instantiate(prefab, transform.position, transform.rotation, transform.parent);
                    if (m_isDestroyAfterReplacing)
                    {
                        Destroy(newObj, m_destroyTime);
                    }
                }                               
            }
            Destroy(gameObject);
        }
    }
}
