﻿using UnityEngine;

public class WallPlacement : MonoBehaviour
{
    [SerializeField] private GameObject m_Walls;
    enum type
    {
        Wood,
        Steel,
        Stone
    }

     public void PlaceWoodWall()
     {
         PlaceWall(m_Walls, type.Wood);
     }

     public void PlaceStoneWall()
     {
         PlaceWall(m_Walls, type.Stone);
     }

     public void PlaceSteelWall()
     {
         PlaceWall(m_Walls, type.Steel);
     }

     private void PlaceWall(GameObject root, type myType)
     {
         foreach (Transform wall in root.transform)
         {
             if (wall.childCount == 0)
             {
                GameObject prefab;

                 switch( myType)
                     {
                     case type.Wood:
                         prefab = wall.GetComponent<WallMaterial>().m_woodWall;
                         break;
                     case type.Stone:
                         prefab = wall.GetComponent<WallMaterial>().m_stoneWall;
                         break;
                    case type.Steel:
                        prefab = wall.GetComponent<WallMaterial>().m_steelWall;
                        break;
                    default:
                        prefab = wall.GetComponent<WallMaterial>().m_woodWall;
                        break;

                }           
                 Instantiate(prefab, wall.position, wall.rotation, wall);
                 break;
             }
         }
     }

}
