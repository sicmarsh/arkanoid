﻿using UnityEngine;

public class SmallShieldActivator : ABonusActivator
{
    public override void Activate(GameObject target)
    {
        if (target.GetComponent<BonusBigShield>())
        {
            Destroy(target.GetComponent<BonusBigShield>());
        }
    }
}
