﻿using UnityEngine;

public class BonusBigShield : ABonus
{
    protected override void Activate()
    {
        var shields = GetComponent<ShieldSwitcher>();
        if(shields != null)
        {
            shields.SwitchShield(true);
        }
    }

    protected override void Deactivate()
    {
        var shields = GetComponent<ShieldSwitcher>();
        if (shields != null)
        {
            shields.SwitchShield(false);
        }
    }
}
