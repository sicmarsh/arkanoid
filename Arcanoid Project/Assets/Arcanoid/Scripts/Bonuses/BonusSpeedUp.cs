﻿using UnityEngine;

public class BonusSpeedUp : ABonus
{
    
    protected override void Activate()
    {
        var platform = GetComponent<PlatformMovement>();
        platform.BonusSpeed(true, BonusAmount);
    }

    protected override void Deactivate()
    {
        var platform = GetComponent<PlatformMovement>();
        platform.BonusSpeed(false, BonusAmount);
    }
}
    