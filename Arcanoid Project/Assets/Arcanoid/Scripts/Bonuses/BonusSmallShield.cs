﻿using UnityEngine;

public class BonusSmallShield : ABonus
{
    protected override void Activate()
    {
        var shields = GetComponent<ShieldSwitcher>();
        if (shields != null)
        {
            shields.SwitchShield(false);
        }
    }

    protected override void Deactivate()
    {       
        var shields = GetComponent<ShieldSwitcher>();
        if (shields != null)
        {
            shields.SwitchShield(true);
        }
    }
}
