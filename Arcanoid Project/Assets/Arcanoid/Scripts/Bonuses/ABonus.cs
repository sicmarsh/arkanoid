﻿using UnityEngine;

public abstract class ABonus : MonoBehaviour
{
    public float Duration { get; set; }
    public float BonusAmount { get; set; }

    protected abstract void Activate();
    protected abstract void Deactivate();

    private void Start()
    {
        Activate();
        if(Duration == 0)
        {
            Duration = Mathf.Infinity;
        }
    }

    private void OnDestroy()
    {
        Deactivate();
    }

    private void Update()   
    {
        Duration -= Time.deltaTime;

        if (Duration < 0)
        {
            Destroy(this);
        }        
    }
}
