﻿using UnityEngine;

public class BigShieldActivator : ABonusActivator
{
    public override void Activate(GameObject target)
    {
        if (target.GetComponent<BonusBigShield>() == null)
        {
            var bonus = target.AddComponent<BonusBigShield>();
            bonus.Duration = m_duration;
        }       
          else
        {
            target.GetComponent<BonusBigShield>().Duration = m_duration;
        }
    }
   
}
