﻿using UnityEngine;

public class PlatformSpeedUpActivator : ABonusActivator
{
    public override void Activate(GameObject target)
    {
        if (target.GetComponent<BonusSpeedUp>() == null)
        {
            var bonus = target.AddComponent<BonusSpeedUp>();
            bonus.BonusAmount = m_bonusAmount;
            bonus.Duration = m_duration;
        }
        else
        {
            target.GetComponent<BonusSpeedUp>().Duration = m_duration;
            target.GetComponent<BonusSpeedUp>().BonusAmount = m_bonusAmount;
        }
    }
}
