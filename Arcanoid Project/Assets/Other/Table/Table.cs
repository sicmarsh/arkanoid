﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table : ATable
{

    public Table(Leg[] leg2)
    {
        legs = leg2;
    }

    override public void Stand()
    {

        if (legs.Length > 3)
        {
            Debug.Log("I stand!");
            HowIStand();
        }
        else
        {
            Debug.Log("I felt");
        }
    }

    public void HowIStand()
    {
        foreach (Leg leg in legs)
        {

            leg.Hold();
        }
    }

}
