﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Starter : MonoBehaviour {

	
	void Start () {
        Leg[] legs = new Leg[]{
            new WoodenLeg(),
            new SteelLeg(),
            new SteelLeg(),
            new BooksInsteadOfLeg()
         };
        Table table = new Table(legs);
        table.Stand();
        
	}
	
	
}
