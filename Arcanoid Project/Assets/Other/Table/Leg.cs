﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Leg
{
    public Table mytable;
    public abstract void Hold();    
}
