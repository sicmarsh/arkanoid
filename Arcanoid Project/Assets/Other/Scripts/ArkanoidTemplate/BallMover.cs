﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMover : MonoBehaviour {
    public GameObject m_ball;
    public float ballSpeed = 30;
    public float jumpForceDivider = 2;
    public Rigidbody rb;
    
    private enum State
    {
        Run,
        Stay
    }
    private State BallState = State.Stay;
    private void Awake()
    {
        rb = m_ball.GetComponent<Rigidbody>();
    }
    private void Start()
    {
        
    }
    void FixedUpdate () {
        if (m_ball == null)
        {
            Debug.Log("BALL IS NULL!!");
            return;
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            
            switch (BallState)
            {
                 
                case State.Stay:                    
                    rb.AddForce(Vector3.forward * ballSpeed, ForceMode.Impulse);
                    BallState = State.Run;
                    return;
                case State.Run:                    
                    rb.AddForce(Vector3.up * ballSpeed/ jumpForceDivider, ForceMode.Impulse);
                    return;
            }
            
        }
        
    }
}
