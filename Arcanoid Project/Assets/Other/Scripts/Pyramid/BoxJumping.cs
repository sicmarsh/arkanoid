﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxJumping : MonoBehaviour {

    public Rigidbody[] m_boxes;
    public Rigidbody m_selectedBox;
    public float force = 200;
    private void FixedUpdate()
    {
        SelectBox();
        if (m_selectedBox == null)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            m_selectedBox.AddForce(Vector3.forward * force);

        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_selectedBox.AddForce(Vector3.up * force);

        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            m_selectedBox.AddForce(Vector3.back * force);

        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            m_selectedBox.AddForce(Vector3.left * force);

        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            m_selectedBox.AddForce(Vector3.right * force);

        }
    }

    private void SelectBox()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            m_selectedBox = m_boxes[0];
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            m_selectedBox = m_boxes[1];
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            m_selectedBox = m_boxes[2];
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            m_selectedBox = m_boxes[3];
        }

        if(m_selectedBox != null)
        {
            Debug.Log("Selected box: " + m_selectedBox.name);
        }
        
    }
   
      
    

}
